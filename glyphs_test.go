package glyphs

import (
	"testing"
)

type glyphDef struct {
	glyph    Glyph
	name     string
	expected rune
}

var definedGlyphs []glyphDef = []glyphDef{
	{glyph: Check, name: "Check", expected: '✓'},
	{glyph: HeavyCheck, name: "HeavyCheck", expected: '✔'},
	{glyph: NoEntry, name: "NoEntry", expected: '⛔'},
	{glyph: Sun, name: "Sun", expected: '☀'},
	{glyph: SunWithCloud, name: "SunWithCloud", expected: '🌤'},
	{glyph: CloudWithSun, name: "CloudWithSun", expected: '⛅'},
	{glyph: Cloud, name: "Cloud", expected: '☁'},
	{glyph: CloudRain, name: "CloudRain", expected: '🌧'},
	{glyph: CloudRainThunder, name: "CloudRainThunder", expected: '⛈'},
}

func TestAsString(t *testing.T) {

	for _, definedGlyph := range definedGlyphs {
		assertWith{t}.thatGlyph(definedGlyph).printAsExpected()
	}
}

type assertWith struct {
	t *testing.T
}

type assertGlyph struct {
	t     *testing.T
	glyph glyphDef
}

func (assert assertWith) thatGlyph(glyph glyphDef) assertGlyph {
	return assertGlyph{
		t:     assert.t,
		glyph: glyph,
	}
}

func (asserted assertGlyph) printAsExpected() {
	assertedGlyph := asserted.glyph
	glyphString := assertedGlyph.glyph.AsString()
	expectedString := string(assertedGlyph.expected)

	if glyphString != expectedString {
		asserted.t.Errorf("'%s' glyph string representation '%s' does not equals expected '%s' (unicode is %U but is expected to be %U)",
			assertedGlyph.name, glyphString, expectedString, assertedGlyph.glyph, assertedGlyph.expected)
	}
}
