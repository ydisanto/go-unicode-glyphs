package glyphs

// type Glyph string
type Glyph rune

const (

	// Commons
	Check      Glyph = '\u2713'
	HeavyCheck Glyph = '\u2714'
	NoEntry    Glyph = '\u26D4'

	// Weather
	Sun              Glyph = '\u2600'
	SunWithCloud     Glyph = '\U0001f324'
	CloudWithSun     Glyph = '\u26C5'
	Cloud            Glyph = '\u2601'
	CloudRain        Glyph = '\U0001F327'
	CloudRainThunder Glyph = '\u26c8'
)

func (glyph Glyph) AsString() string {
	return string(glyph)
}
